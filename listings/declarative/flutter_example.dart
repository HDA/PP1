import 'package:flutter/material.dart';

class ButtonExample extends StatefulWidget {
  @override
  _ButtonExampleState createState() => _ButtonExampleState();
}

class _ButtonExampleState extends State<ButtonExample> {
  bool gotClicked = false;
  @override
  Widget build(BuildContext context) {
    return FlatButton(
      child: Text(gotClicked ? "Clicked" : "Not clicked"),
      onPressed: () => setState(() { gotClicked = false; }),
    );
  }
}