class DepartureAdapter(private val departures: List<Departure>): RecyclerView.Adapter<DepartureAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_departure, parent, false)
        )
    }

    override fun getItemCount(): Int = departures.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(departures[position])
    }

    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        fun bind(departure: Departure) = with(itemView) {
            trainLine.text = departure.train.name
            trainDestination.text = departure.finalDestination
            actualPlatform.text = departure.info.platform?.let { "Gl. $it" }
            if(departure.info.hasDelay) {
                val parsedActualTime = DateFormat
                    .getTimeInstance(DateFormat.SHORT)
                    .format(departure.info.actualTime)
                actualTime.text = "vrs. ${parsedActualTime}"
            } else {
                actualTime.visibility = View.GONE
            }
            plannedTime.text = DateFormat
                .getTimeInstance(DateFormat.SHORT)
                .format(departure.info.scheduledTime)
        }
    }
}